import React from 'react';
import { LineChart, Line, BarChart, Bar, XAxis, YAxis } from 'recharts';
import Sales from '../Components/Sales'

fetch('../cors.php')
  .then(res => console.log(res))

const data1 = [{name: 'Page A', uv: 200, pv: 2400, amt: 2400}, {name: 'Page B', uv: 100, pv: 3400, amt: 3000}, {name: 'Page B', uv: 100, pv: 3400, amt: 3000}, {name: 'Page B', uv: 100, pv: 3400, amt: 3000}, ];
const data2 = [{name: 'Page A', uv: 400, pv: 2400, amt: 2400}, {name: 'Page B', uv: 500, pv: 2500, amt: 2500}, {name: 'Page C', uv: 600, pv: 2600, amt: 2600}, {name: 'Page D', uv: 200, pv: 1800, amt: 3000}, {name: 'Page E', uv: 400, pv: 2400, amt: 2400}];

const renderCustomAxisTick = ({ payload }) => {
  let path = '';

  switch (payload.value) {
    case 'Page A':
      path = 'M899.072 99.328q9.216 13.312 17.92 48.128t16.384 81.92 13.824 100.352 11.264 102.912 9.216 90.112 6.144 60.928q4.096 30.72 7.168 70.656t5.632 79.872 4.096 75.264 2.56 56.832q-13.312 16.384-30.208 25.6t-34.304 11.264-34.304-2.56-30.208-16.896q-1.024-10.24-3.584-33.28t-6.144-53.76-8.192-66.56-8.704-71.68q-11.264-83.968-23.552-184.32-7.168 37.888-11.264 74.752-4.096 31.744-6.656 66.56t-0.512 62.464q1.024 18.432 3.072 29.184t4.608 19.968 5.12 21.504 5.12 34.304 5.12 56.832 4.608 90.112q-11.264 24.576-50.688 42.496t-88.576 29.696-97.28 16.896-74.752 5.12q-18.432 0-46.08-2.56t-60.416-7.168-66.048-12.288-61.952-17.92-49.664-24.064-28.16-30.208q2.048-55.296 5.12-90.112t5.632-56.832 5.12-34.304 5.12-21.504 4.096-19.968 3.584-29.184q2.048-27.648-0.512-62.464t-6.656-66.56q-4.096-36.864-11.264-74.752-13.312 100.352-24.576 184.32-5.12 35.84-9.216 71.68t-8.192 66.56-6.656 53.76-2.56 33.28q-13.312 12.288-30.208 16.896t-34.304 2.56-33.792-11.264-29.696-25.6q0-21.504 2.048-56.832t4.096-75.264 5.632-79.872 6.656-70.656q2.048-20.48 6.144-60.928t9.728-90.112 11.776-102.912 13.824-100.352 16.384-81.92 17.92-48.128q20.48-12.288 56.32-25.6t73.216-26.624 71.168-25.088 50.176-22.016q10.24 13.312 16.896 61.44t13.312 115.712 15.36 146.432 23.04 153.6l38.912-334.848-29.696-25.6 43.008-54.272 15.36 2.048 15.36-2.048 43.008 54.272-29.696 25.6 38.912 334.848q14.336-74.752 23.04-153.6t15.36-146.432 13.312-115.712 16.896-61.44q16.384 10.24 50.176 22.016t71.168 25.088 73.216 26.624 56.32 25.6';
      break;
    default:
      path = '';
  }
}

const renderLineChart = (
  <LineChart width={400} height={400} data={data2}>
    <Line type="monotone" dataKey="uv" stroke="#8884d8" />
  </LineChart>
)

const renderCustomBarLabel = ({x, y, width, value }) => {
  return <text x={x + width / 2} y={y} fill="#666" textAnchor="middle" dy={-6}>{`value: ${value}`}</text>;
};

const renderBarChart = (
  <BarChart width={600} height={300} data={data1}>
    <XAxis dataKey="name" tick={renderCustomAxisTick} />
    <YAxis />
    <Bar dataKey="uv" barSize={30} fill="#8884d8"
      label={renderCustomBarLabel}/>
  </BarChart>
);

class Business extends React.Component {

  componentDidMount() {
    fetch("https://brandfirm.simplicate.nl/api/v2/sales/sales", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Authentication-Secret": "UnAJoWSnuCI7QoqN3APc39wTPRe8D9Qo",
        "Authentication-Key": "y9tSZavi8IUvNnQs6PjciCtt0QvutV4R"
      }
    })
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            "We hebben een probleeem. Status Code: " + response.status
          );
          return;
        }
        response.json().then(function(data) {
          console.log(data);
        });
      })
      .catch(function(err) {
        console.log("Fetch Error :-S", err);
      });
  }

  render() {

    var { isLoaded, sales } = this.state

    if (!isLoaded) {
      return <div>Loading...</div>
    } else {
      return (
        <div>
          <div className="business__container">
            <div className="business__box business__box--sales">
            <p>Sales</p> 
            </div>
            <div className="business__box business__box--omzet">
              <p>Omzet</p> 
            </div>
            <div className="business__box business__box--projecten">
              <p>Actieve Projecten</p> 
            </div>
          </div>
          {Sales}
  
          <div className="business__container">
            <div className="business__box business__box--ttlsales">
            <p>Totale Sales</p> 
            {renderLineChart}
            </div>
            <div className="business__box business__box--wklomzet">
              <p>Wekelijkse omzet</p> 
              {renderBarChart}
            </div>
          </div>
        </div>
      )
    }  
  }
}

export default Business;
