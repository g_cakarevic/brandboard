import $ from 'jquery';

const Instagram = require('instagram-web-api')
const FileCookieStore = require('tough-cookie-filestore2')

const { username, password, followers } = process.env

const cookieStore = new FileCookieStore('./cookies. json')
const client = new Instagram({ username, password, cookieStore })

(async () => {
  const photo =
    'https://scontent-scl1-1.cdninstagram.com/t51.2885-15/e35/22430378_307692683052790_5667315385519570944_n.jpg'

  await client.login()

  const { media } = await client.uploadPhoto(photo)
  console.log(`https://www.instagram.com/p/${media.code}/`)
  })

class Instagram extends React.Component {
  render() {
    return (
      <div>{client}</div>
    )
  }
}

export default Instagram;