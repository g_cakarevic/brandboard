import React from 'react';

const BASE_URL = 'https://brandfirm.simplicate.nl/'

class SimplicateAPI extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      omzetData: [], 
      salesData: [],
      actieveProjectenData: [],
      totaleSalesData: [],
      wekelijkseOmzetData: []
    };
  }

  componentDidMount() {
    const proxyurl = "https://cors-anywhere.herokuapp.com/";
    fetch(proxyurl, BASE_URL, {
      method: "POST", 
      withCredentials: true, 
      headers: {
        "X-Auth-Key": "y9tSZavi8IUvNnQs6PjciCtt0QvutV4R", 
        "X-Auth-Secret": "UnAJoWSnuCI7QoqN3APc39wTPRe8D9Qo"
      }
    })
    .then(resp => resp.json())
    .then(json => {
      this.setState({
        isLoaded: true, 
        omzetData: [], 
        salesData: [],
        actieveProjectenData: [],
        totaleSalesData: [],
        wekelijkseOmzetData: []
      })
    });
  }

  render() {
    const { isLoaded } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="Test">
          Data has been loaded
        </div>
      );
    }
  }
}

export default SimplicateAPI;
