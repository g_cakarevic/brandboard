const proxy = reuquire("http-proxy-middleware");

module.exportsa = function(app) {
    app.use (
        proxy("/api/v2/sales/sales", {
            target: "https://brandfirm.simplicate.nl/",
            secure: false,
            changeOrigin: true
        })
    );
};